<?php

namespace app\repositories\user;

use app\models\User;

class UserRepository
{
    public function findByUsername($userName)
    {
        return User::findOne(['username' => $userName]);
    }

    public function findActiveByUsername($userName)
    {
        return User::findOne(
            ['username' => $userName, 'status' => User::USER_STATUS_ACTIVE]
        );
    }

    public function getList()
    {
        return User::find()->all();
    }

    public function findById($id)
    {
        return User::findOne(['id' => $id]);
    }
}