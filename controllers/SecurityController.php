<?php

namespace app\controllers;

use app\services\security\AuthenticationManager;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;

class SecurityController extends Controller
{
    private $authenticationManager;

    public function __construct(
        $id,
        $module,
        AuthenticationManager $authenticationManager,
        $config = []
    ) {
        $this->authenticationManager = $authenticationManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new LoginForm();

        if ($this->authenticationManager->login($form)) {
            return $this->goBack();
        } else {
            $form->password = '';

            return $this->render(
                'login',
                [
                    'model' => $form,
                ]
            );
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $this->authenticationManager->logout();

        return $this->goHome();
    }
}