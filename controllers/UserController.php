<?php

namespace app\controllers;

use app\models\forms\AddUserForm;
use app\models\forms\UpdateUserForm;
use app\services\user\UserManager;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class UserController extends Controller
{
    private $userManager;

    public function __construct(
        $id,
        $module,
        UserManager $userManager,
        $config = []
    ) {
        $this->userManager = $userManager;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['add', 'update', 'delete'],
                        'roles'   => ['admin'],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'roles'   => ['user'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $list = $this->userManager->listUser();

        return $this->render('index', compact('list'));
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $user = $this->userManager->oneUser($id);

        return $this->render('view', ['user' => $user]);
    }

    /**
     * @return string
     */
    public function actionAdd()
    {
        $form = new AddUserForm();
        $newUser = $this->userManager->addUser($form);
        if ($newUser) {
            return $this->redirect(['view', 'id' => $newUser->id]);
        } else {
            return $this->render(
                'add',
                [
                    'model' => $form,
                ]
            );
        }
    }

    /**
     * @return string
     */
    public function actionUpdate($id)
    {
        $user = $this->userManager->oneUser($id);
        $form = new UpdateUserForm($user);
        if ($this->userManager->updateUser($form)) {
            return $this->redirect(['view', 'id' => $user->id]);
        } else {
            return $this->render(
                'update',
                [
                    'model' => $form,
                ]
            );
        }
    }

    /**
     * @return string
     */
    public function actionDelete($id)
    {
        $user = $this->userManager->oneUser($id);

        if ($this->userManager->deleteUser($user)) {
            return $this->redirect(['index']);
        } else {
            throw new \Exception("Cant delete user!");
        }
    }
}
