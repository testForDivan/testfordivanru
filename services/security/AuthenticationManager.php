<?php

namespace app\services\security;

use app\models\forms\LoginForm;
use app\repositories\user\UserRepository;
use Yii;

class AuthenticationManager
{
    const REMEMBER_ME_TIME = 3600 * 24 * 30;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(LoginForm $form)
    {
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $user = $this->userRepository->findActiveByUsername(
                $form->username
            );

            if ($user) {
                return Yii::$app->user->login(
                    $user,
                    $form->rememberMe ? $this::REMEMBER_ME_TIME : 0
                );
            } else {
                $form->addError('username', 'Доступ запрещен!');
            }
        }

        return null;
    }

    public function logout()
    {
        Yii::$app->user->logout();
    }
}