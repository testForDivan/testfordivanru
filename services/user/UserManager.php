<?php

namespace app\services\user;

use app\models\forms\AddUserForm;
use app\models\forms\LoginForm;
use app\models\forms\UpdateUserForm;
use app\models\User;
use app\repositories\user\UserRepository;
use Yii;
use yii\base\Theme;
use yii\data\ArrayDataProvider;

class UserManager
{
    const DEFAULT_NEW_USER_STATUS = User::USER_STATUS_DISACTIVE;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function listUser()
    {
        return $this->wrapIntoDataProvider($this->userRepository->getList());
    }

    private function wrapIntoDataProvider($data)
    {
        return new ArrayDataProvider(
            [
                'key'        => 'id',
                'allModels'  => $data,
                'pagination' => false,
            ]
        );
    }

    public function oneUser($id)
    {
        return $this->userRepository->findById($id);
    }

    public function addUser(AddUserForm $form)
    {
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $user = new User();
            $user->username = $form->username;
            $user->password = $this->generateHashPassword($form->password);
            $user->setAuthKey($this->generateAuthKey());
            $user->status = $this::DEFAULT_NEW_USER_STATUS;

            $user->save();

            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('user');
            $auth->assign($authorRole, $user->getId());

            return $user;
        }

        return null;
    }

    private function generateHashPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }

    private function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }

    public function updateUser(UpdateUserForm $form)
    {
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            /** @var $user User */
            $user = $this->userRepository->findById($form->id);

            if ($user) {
                $user->status = $form->status;
                $user->username = $form->username;
                if ($form->newPassword) {
                    $user->password = $this->generateHashPassword(
                        $form->newPassword
                    );
                }

                return $user->save();
            }
        }

        return null;
    }

    public function deleteUser(User $user)
    {
        return $user->delete();
    }
}