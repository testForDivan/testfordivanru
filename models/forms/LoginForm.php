<?php

namespace app\models\forms;

use app\validators\user\PasswordValidator;
use yii\base\Model;

class LoginForm extends Model
{
    public $username;

    public $password;

    public $rememberMe;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', PasswordValidator::class],
        ];
    }
}