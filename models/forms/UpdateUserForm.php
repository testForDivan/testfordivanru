<?php

namespace app\models\forms;

use app\models\User;
use app\validators\user\ChangeUsernameValidator;
use yii\base\Model;

class UpdateUserForm extends Model
{
    public $id;

    public $username;

    public $newPassword;

    public $status;

    public function __construct(User $user, array $config = [])
    {
        $this->id = $user->getId();
        $this->username = $user->username;
        $this->status = $user->status;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['id'], 'required'],
            [['username'], 'required'],
            [['username'], ChangeUsernameValidator::class],
            [['newPassword'], 'safe'],
            [['status'], 'safe'],
        ];
    }
}