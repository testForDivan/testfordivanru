<?php

namespace app\models\forms;

use yii\base\Model;

class AddUserForm extends Model
{
    public $username;

    public $password;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [
                ['username'],
                'unique',
                'targetClass' => '\app\models\User',
                'message'     => 'Пользователь с этим именем уже существует.',
            ],
        ];
    }
}