<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password
 * @property string  $status
 * @property string  $auth_key
 *
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    const USER_STATUS_ACTIVE = 1;

    const USER_STATUS_DISACTIVE = 0;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function setAuthKey($authKey)
    {
        $this->auth_key = $authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function getRole()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        $result = [];

        foreach ($roles as $role) {
            if ($role->description) {
                $result[] = $role->description;
            }
        }

        return implode(',', $result);
    }
}