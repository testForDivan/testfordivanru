<?php

namespace app\validators\user;

use Yii;
use app\repositories\user\UserRepository;
use yii\validators\Validator;

class PasswordValidator extends Validator
{
    private $userRepository;

    public function __construct(UserRepository $userRepository,$config=[])
    {
        $this->userRepository = $userRepository;
        parent::__construct($config);
    }

    public function validateAttribute($model, $attribute)
    {
        $user = $this->userRepository->findByUsername($model->username);
        if (!$user || !Yii::$app->security->validatePassword($model->password, $user->password)) {
            $this->addError($model,$attribute, 'Неверный логин или пароль!');
        }
    }

}