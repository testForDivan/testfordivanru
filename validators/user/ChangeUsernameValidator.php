<?php

namespace app\validators\user;

use Yii;
use app\repositories\user\UserRepository;
use yii\validators\Validator;

class ChangeUsernameValidator extends Validator
{
    private $userRepository;

    public function __construct(UserRepository $userRepository, $config = [])
    {
        $this->userRepository = $userRepository;
        parent::__construct($config);
    }

    public function validateAttribute($model, $attribute)
    {
        $user = $this->userRepository->findByUsername($model->username);

        if ($user && $user->getId() != $model->id) {
            $this->addError(
                $model,
                $attribute,
                'Пользователь с этим именем уже существует!'
            );
        }
    }

}