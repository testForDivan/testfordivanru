
Задачи:
1. Создать и запустить приложение на фреймворке Yii2. Реализовать  авторизацию и управление пользователями в БД.
2. Реализовать функционал по ресайзу и наложению ватермарки на прозрачную анимированную гифку.
3. реализовать плагин jquery "follow", с помощью которого элементы приобретают следующее поведение: при наведении курсора мыши цепляются к ней и следуют за ее перемещениями, до клика.
