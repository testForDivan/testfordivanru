<?php

use yii\helpers\Html;

$this->title = 'User' . $user->id;
$this->params['breadcrumbs'][] = ['label' => 'User list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?php if (\Yii::$app->user->can('admin')) {?>
        <?= Html::a('Редактировать', ['update', 'id' => $user->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $user->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    <?php }?>
</p>

<?= \yii\widgets\DetailView::widget([
    'model' => $user,
    'attributes' => [
        'id',
        'username',
        'status'
    ],
]) ?>
