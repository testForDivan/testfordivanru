<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'User list';
$this->params['breadcrumbs'][] = $this->title;
if(Yii::$app->user->can('admin')) {
    $actions='{view} {update} {delete}';
}
else {
    $actions='{view}';
}
?>
<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?php if (\Yii::$app->user->can('admin')) {?>
        <?= Html::a('Добавить пользователя', ['add'], ['class' => 'btn btn-success']) ?>
    <?php }?>
</p>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $list,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'username',
        'status',
        'role',
        [
            'class' => 'yii\grid\ActionColumn',
//            'template' => $actions,
            'visibleButtons'=>[
                'view' => \Yii::$app->user->can('user'),
                'update' => \Yii::$app->user->can('admin'),
                'delete' => \Yii::$app->user->can('admin')
            ]
        ],
    ],
]); ?>






