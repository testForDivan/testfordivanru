<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'User update';
$this->params['breadcrumbs'][] = ['label' => 'User list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'id')->hiddenInput() ?>

            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

