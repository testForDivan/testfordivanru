<?php

use yii\db\Migration;
use \app\models\User;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190309_202218_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull()->unique(),
            'status' => $this->boolean()->notNull()->defaultValue(User::USER_STATUS_ACTIVE)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
