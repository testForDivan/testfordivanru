<?php

use yii\db\Migration;

/**
 * Class m190310_231051_create_roles
 */
class m190310_231051_create_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rbac = Yii::$app->authManager;

        $guest = $rbac->createRole('guest');
        $guest->description = 'Гость';
        $rbac->add($guest);

        $user = $rbac->createRole('user');
        $user->description = 'Пользователь';
        $rbac->add($user);

        $admin = $rbac->createRole('admin');
        $admin->description = 'Администратор';
        $rbac->add($admin);

        $rbac->addChild($admin, $user);
        $rbac->addChild($user, $guest);
        $rbac->assign(
            $admin,
            \app\models\User::findOne(['username' => 'admin'])->id
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $manager = Yii::$app->authManager;
        $manager->removeAll();
    }
}
